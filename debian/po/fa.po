# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Persian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# , 2005.
msgid ""
msgstr ""
"Project-Id-Version: fa\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2010-07-29 02:08+0330\n"
"Last-Translator: Ebrahim Byagowi <ebraminio@gmail.com>\n"
"Language-Team: Debian-l10n-persian <debian-l10n-persian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid "Partition for boot loader installation:"
msgstr "پارتیشن برای نصب بوت لودر:"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid ""
"Partitions currently available in your system are listed. Please choose the "
"one you want elilo to use to boot your new system."
msgstr ""
"پارتیشن‌هایی که اکنون در سیستم شما موجود است لیست شده است. لطفا یکی را برای "
"انجام عملیات بوت سیستم جدید انتخاب کنید."

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid "No boot partitions detected"
msgstr "پارتیشن قابل بوت یافت نشد"

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid ""
"There were no suitable partitions found for elilo to use.  Elilo needs a "
"partition with a FAT file system, and the boot flag set."
msgstr ""
"پارتیشن مناسب برای استفاده elilo پیدا نشد. Elilo به یک پارتیشن با فرمت FAT "
"نیاز دارد و boot flag آن هم باید فعال شده باشد."

#. Type: text
#. Description
#. Main menu item
#: ../elilo-installer.templates:3001
msgid "Install the elilo boot loader on a hard disk"
msgstr "نصب بوت لودر elilo بر روی دیسک سخت"

#. Type: text
#. Description
#: ../elilo-installer.templates:4001
msgid "Installing the ELILO package"
msgstr "نصب بسته ELILO"

#. Type: text
#. Description
#: ../elilo-installer.templates:5001
msgid "Running ELILO for ${bootdev}"
msgstr "اجرای ELILO برای ${bootdev}"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid "ELILO installation failed.  Continue anyway?"
msgstr "نصب ELILO با شکست مواجه شد. به هر حال ادامه داده شود؟"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid ""
"The elilo package failed to install into /target/.  Installing ELILO as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to ELILO, so continuing the installation may be possible."
msgstr ""
"نصب elilo در /target/ با شکست مواجه شد. نصب ELILO به عنوان boot loader یک "
"مرحله ضروری است. مشکل در نصب ممکن است مربوط به ELILO نباشد، بنابراین احتمال "
"دارد ادامه نصب امکان پذیر باشد."

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "ELILO installation failed"
msgstr "نصب ELILO با شکست مواجه شد."

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "Running \"/usr/sbin/elilo\" failed with error code \"${ERRCODE}\"."
msgstr "اجرای \"/usr/sbin/elilo\" با خطای  \"${ERRCODE}\" متوقف شد."
