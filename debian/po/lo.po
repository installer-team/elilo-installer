# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of lo.po to Lao
# Lao translation of debian-installer.
# Copyright (C) 2006-2010 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Anousak Souphavanh <anousak@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: lo\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2010-11-26 09:11+0700\n"
"Last-Translator: Anousak Souphavanh <anousak@gmail.com>\n"
"Language-Team: Lao <lo@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid "Partition for boot loader installation:"
msgstr "ພາຣທິຊັນທີ່ຈະຕິດຕັ້ງບູຕໂຫລດເດີ:"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid ""
"Partitions currently available in your system are listed. Please choose the "
"one you want elilo to use to boot your new system."
msgstr ""
"ຕໍ່ໄປນີ້ເປັນລາຍການຂອງພາທິຊັນທີ່ມີໃນລະບົບຂອງທ່ານ ກະລຸນາເລືອກພາທິຊັນທີ່ຕ້ອງການໃຫ້ ELILO "
"ໃຊ້ບູຕລະບົບຂອງທ່ານ."

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid "No boot partitions detected"
msgstr "ບໍ່ເຫັນພາທິຊັນສຳຫລັບບູຕ"

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid ""
"There were no suitable partitions found for elilo to use.  Elilo needs a "
"partition with a FAT file system, and the boot flag set."
msgstr ""
"ບໍ່ມີພາທິຊັນທີ່ເໝາະສົມສຳຫລັບໃຫ້ elilo ຕ້ອງໃຊ້ພາທິຊັນທີ່ມີລະບົບແຟ້ມແບບ FAT "
"ແລະກຳນົດແຟລັກໃຫ້ບູຕໄດ້ເອົາໄວ້."

#. Type: text
#. Description
#. Main menu item
#: ../elilo-installer.templates:3001
msgid "Install the elilo boot loader on a hard disk"
msgstr "ຕິດຕັ້ງບູຕໂຫລດເດີ elilo ລົງໃນຮາດດີສ "

#. Type: text
#. Description
#: ../elilo-installer.templates:4001
msgid "Installing the ELILO package"
msgstr "ກຳລັງຕິດຕັ້ງແພກເກັດ elilo "

#. Type: text
#. Description
#: ../elilo-installer.templates:5001
msgid "Running ELILO for ${bootdev}"
msgstr "ກຳລັງດຳເນີນການ ELILO ສຳຫລັບ ${bootdev}"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid "ELILO installation failed.  Continue anyway?"
msgstr "ຕິດຕັ້ງ ELILO ບໍ່ສຳເລັດຈະດຳເນີນການຕໍ່ໄປຫລືບໍ່?"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid ""
"The elilo package failed to install into /target/.  Installing ELILO as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to ELILO, so continuing the installation may be possible."
msgstr ""
"ຕິດຕັ້ງແພກເກັດ elilo ລົງໃນ /target/ ບໍ່ສຳເລັດ ການຕິດຕັ້ງ ELILO ເປັນບູຕໂຫລດເດີ "
"ເປັນຂັ້ນຕອນທີ່ຈຳເປັນ ແຕ່ປັນຫາຂອງການຕິດຕັ້ງອາດບໍ່ກ່ຽວກັບ ELILO ກໍ່ໄດ້ ດັ່ງນັ້ນ "
"ການດຳເນີນການຕິດຕັ້ງຕໍ່ໄປກໍ່ອາດເຮັດໄດ້."

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "ELILO installation failed"
msgstr "ຕິດຕັ້ງ ELILO ບໍ່ສຳເລັດ"

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "Running \"/usr/sbin/elilo\" failed with error code \"${ERRCODE}\"."
msgstr "ການໃຊ້ \"/usr/sbin/elilo\" ບໍ່ສຳເລັດ ໂດຍມີລະຫັດຂໍ້ຜິດພາດ \"${ERRCODE}\""
